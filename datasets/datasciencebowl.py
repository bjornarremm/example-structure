import glob
import cv2

import numpy as np
from skimage.color import rgb2gray
from skimage.io import imread
import skimage
import torch
import torchvision
from torch.utils.data import Dataset

from utils import nn as netutils

from PIL import Image
TRAIN_PATH = 'data-science-bowl-2018/stage1_train/'
TRAIN_PATH_SEARCH = TRAIN_PATH + "*"
TRAIN_PATH_IMAGES = TRAIN_PATH_SEARCH + "/images/*"
TRAIN_PATH_MASKS = TRAIN_PATH_SEARCH + "/masks/*"

TEST_PATH = 'data-science-bown-2018/stage1_test/'
TEST_PATH_SEARCH = TEST_PATH + "*"
TEST_PATH_IMAGES = TEST_PATH_SEARCH + "/images/*"
TEST_PATH_MASKS = TEST_PATH_SEARCH + "/masks/*"


class DataScienceBowl(Dataset):

    def __init__(self, config):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        for key, value in config.items():
            if key in ["IMAGE_PATHS", "transform", "image_size", "border_type",
                       "border_color", "padding", "grayscale"]:
                setattr(self, key, value)
            else:
                raise ValueError("key: {0}".format(key) + " should not be part of the program.")
        self.path_images = glob.glob(self.IMAGE_PATHS)
        self.scale = torchvision.transforms.Compose([torchvision.transforms.ToTensor])
        self.override_len = None



    def __len__(self):

        if self.override_len is not None:
            return self.override_len
        else:
            return len(self.path_images)

    def __getitem__(self, item):
        IMAGE_PATH = self.path_images[item]
        MASK_PATHS = "/".join(IMAGE_PATH.split("/")[0:-2]) + "/masks/*.png"
        masks = skimage.io.imread_collection(MASK_PATHS).concatenate()
        mask = np.array(np.max(masks, axis=0), dtype=np.float64)

        if self.padding:
            image = torch.from_numpy(netutils.padding(rgb2gray(imread(IMAGE_PATH, as_grey=self.grayscale)), self.image_size, self.border_type, self.border_color)).float().div(255)
            mask = torch.from_numpy(netutils.padding(rgb2gray(mask), self.image_size, self.border_type, self.border_color)).float().div(255)

        else:
            image = torch.from_numpy(np.expand_dims(imread(IMAGE_PATH, as_grey=self.grayscale))).float()

        return {"image": image, "mask": mask}






if __name__ == "__main__":
    OPTIONS = {
                "IMAGE_PATHS":"data-science-bowl-2018/stage1_train/*/images/*.png",
                "transform":None,
                "image_size":256,
                "border_type":cv2.BORDER_CONSTANT,
                "border_color":[0,0,0],
                "padding":True,
                "grayscale":True
            }

    ds = DataScienceBowl(OPTIONS)
    ALL_IMAGES = len(ds)
    for i in range(ALL_IMAGES):
        x = ds[i]
        print("-"*60)
        print("-----" + " " + str(i) + " of " + str(ALL_IMAGES) + " -----")
        print("image: " +str(x["image"].shape))
        print("mask: " +str(x["mask"].shape))
        print("-"*60)


    print("Completed test")
