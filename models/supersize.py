import torch
import torch.nn as nn



class ImageSR(nn.Module):
    """
    This is a network to learn more about how to scale pictures up.
    Could be used to rescale a picture to a larger size..
    For example from 64x64 pixels to 128x128.

    """
    def __init__(self):
        super(ImageSR, self).__init__()

        self.input_channels = 3
        self.out_channels1 = 16
        self.out_channels2 = 32

     #   super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(self.input_channels, self.out_channels1, kernel_size=5, stride=1, padding=2),
            #nn.BatchNorm2d(self.out_channels1),
            nn.ReLU()
            #nn.MaxPool2d(kernel_size=2, stride=2)
        )
        self.layer2 = nn.Sequential(
            nn.Conv2d(self.out_channels1, self.out_channels2, kernel_size=5, stride=1, padding=2),
            #nn.BatchNorm2d(self.out_channels2),
            nn.ReLU()
            #nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.layer3 = nn.Sequential(
            nn.ConvTranspose2d(32, 64, kernel_size=2, padding=0, output_padding=0, stride=2),
            nn.ReLU()

        )
        self.layer4 = nn.Sequential(
            nn.ConvTranspose2d(64, 128, kernel_size=2, padding=0, output_padding=0, stride=2),
            nn.ReLU()

        )
        self.layer5 = nn.Sequential(
            nn.ConvTranspose2d(128, 3, kernel_size=2, padding=0, output_padding=0, stride=2),
            nn.ReLU()



        )


    def forward(self, x):
        print(x.shape)
        out = self.layer1(x)
        print(out.shape)
        out = self.layer2(out)
        print(out.shape)
        out = self.layer3(out)
        print(out.shape)
        out = self.layer4(out)
        print(out.shape)
        out = self.layer5(out)
        return out


if __name__ == "__main__":

    model = ImageSR()
    x = torch.randn(1, 3,256,256)
    y_pred = model(x)
    print(y_pred.shape)