import numpy as np
from torch.autograd import Variable
import torch
import cv2
import os
import shutil

"""
General functions to help speed up the process of training and testing
This is ment for general purpose use in pytorch

"""


def save_checkpoint(state, is_best, filename='checkpoint.pth.tar', folder="models"):
    if not os.path.exists(folder):
        os.makedirs(folder)

    torch.save(state, os.path.join(folder, filename))
    if is_best:
        shutil.copyfile(os.path.join(folder, filename), os.path.join(folder, 'model_best.pth.tar'))


def padding(im, desired_size, border_type = cv2.BORDER_CONSTANT, color = [0, 0, 0]):
    """
    :param desired_size: 256 by 256 for example
    :param border_type: constant black
    :param color: black
    :return: c, w, h
    """
    old_size = im.shape[:2]  # old_size is in (height, width) format
    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format
    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = color
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, border_type,
                                value=color)

    new_im = np.expand_dims(new_im, axis=0)
    return new_im

def train_epoch(epoch, log_interval, model, data_loader, optimizer, gpu, keys=[]):
    model.train()
    pid = os.getpid()
    for batch_idx, x in enumerate(data_loader):
        data, target = x[keys[0]], x[keys[1]]
        if gpu:
            data, target = Variable(data).cuda().double(), Variable(target).cuda().double()
        else:
            data, target = Variable(data).double(), Variable(target).double()
        optimizer.zero_grad()
        output = model(data)
        m = torch.nn.Sigmoid()
        loss = F.binary_cross_entropy(m(output), target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('{}\tTrain Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                pid, epoch, batch_idx * len(data), len(data_loader.dataset),
                100. * batch_idx / len(data_loader), loss.data[0]))